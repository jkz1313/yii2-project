<?php
return [
    'cookieValidationKey' => '',
    'cookieDomain' => '.yii2-project.ru',
    'frontendHostInfo' => 'http://yii2-project.ru',
    'backendHostInfo' => 'http://yii2-project.ru/admin',
];
